class Audition < ApplicationRecord
    
    belongs_to :user
    belongs_to :part
    
    has_many :audio_takes
    has_many :comments, through: :audio_takes
    has_one :project, through: :part

    # TODO add_column: submitted = true/false

    # files helper method

    api_accessible :audition do |t|
        t.add :id
        t.add -> (a) { a.user.username }, as: :actor_name # 'actor_name' is duplicate data
        t.add -> (a) { a.part.character_name }, as: :character_name #duplicate data
        # t.add template: :take
        t.add :created_at
    end

    api_accessible :saved_audition do |t|
        t.add :id
        t.add :user_id
        t.add :part_id
        t.add :created_at
        t.add :audio_takes, template: :audio_takes
    end

    api_accessible :submitted_audition do |t|
        
    end

end
