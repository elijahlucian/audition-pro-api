class Part < ApplicationRecord
    has_one :user
    belongs_to :project
    has_many :lines

    api_accessible :parts do |t|
        t.add :id
        t.add :project_id
        # t.add -> (p) { p.project.name }, as: :project_name
        t.add :character_name
        t.add :character_profile
        t.add :voice_type
        t.add :age
        t.add :accent
        t.add :notes
        t.add :reference
        t.add :lines, template: :lines
    end

end
