class Project < ApplicationRecord
    belongs_to :user
    has_many :parts

    api_accessible :projects do |t|
        t.add :id
        t.add :name
        t.add :synopsis
        t.add -> (p) { p.user.username }, as: :author
        t.add :parts, template: :parts
    end

end