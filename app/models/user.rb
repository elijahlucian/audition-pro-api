class User < ApplicationRecord
    has_many :auditions
    has_many :projects #, foreign_key: 'user_id'

    api_accessible :usernames do |t|
        t.add :id
        t.add :username
        t.add -> (u) { u.kind.capitalize }, as: :kind
    end

    api_accessible :actor_list, extend: :usernames do |t|
    end

    api_accessible :creator_list, extend: :usernames do |t|
    end
    
    api_accessible :user do |t|
        t.add :id
        t.add :username
        t.add :email
        t.add -> (u) { u.kind.capitalize }, as: :kind
        t.add :auditions, template: :saved_audition
    end

end

