class ApplicationRecord < ActiveRecord::Base
  acts_as_api
  
  self.abstract_class = true
end
