class AudioTake < ApplicationRecord
    belongs_to :audition
    has_many :comments

    api_accessible :audio_takes do |t|
        t.add :id
        t.add :file_path
        t.add :line_id
        t.add :comments, template: :comments
    end

end
