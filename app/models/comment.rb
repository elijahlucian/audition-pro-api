class Comment < ApplicationRecord
    belongs_to :audio_take
    belongs_to :user
    has_one :audition, through: :audio_take

    api_accessible :comments do |t|
        t.add :id
        t.add -> (c) { c.user.username }, as: :author
        t.add :user_id, as: :author_id
        t.add :audio_take_id
        t.add -> (c) { c.audition.id }, as: :audition_id
        t.add :body
    end

end
