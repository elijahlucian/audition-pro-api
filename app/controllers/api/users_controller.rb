class Api::UsersController < ApplicationController

    #todo - get_users, get_actors

    def index
        render_object(User.all.as_api_response(:usernames))
    end
    
    def login

    end

    def actor_list
        @users = User.where(kind: 'actor')
        render_object(@users.as_api_response(:actor_list))
    end
    
    def creator_list
        @users = User.where(kind: 'creator')
        render_object(@users.as_api_response(:creator_list))
    end

    def show
        p params
        @user = User.find(params[:user_id])
        render_object(@user.as_api_response(:user))
    end

end
