class Api::PartsController < ApplicationController

    def index
        @parts = Part.all
        render_object(@parts.as_api_response(:parts))    
    end

end
