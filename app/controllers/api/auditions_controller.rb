class Api::AuditionsController < ApplicationController
    skip_before_action :verify_authenticity_token  

    require 'wavefile'
    include WaveFile

    # get_resource = @auditions
    
    def index # get all auditions
        
        render_object(get_resource.as_api_response(:audition))
    end

    def show
        @audition = get_resource.find_by(part_id: params[:part_id])
        p @audition
        if @audition.submitted
            # render submitted audition
        else
            render_object(@audition.as_api_response(:saved_audition))
        end
    end

    def auditions_by_actor
        @auditions = Audition.where(actor_id: params[:actor_id])
        # get auditions by :actor_id
        # nest by part
    end
    
    def audition_by_part # creator endpoint 
        # dashboard renders all auditions for a specific part.
        # nest all the lines and takes within this 

        # [
        #     1: [{},{},{}], 
        #     2, 
        #     3, 
        #     4, 
        #     5 
        # ]

        @auditions = Audition.where(part_id: params[:part_id])
       
        
        render_object @auditions
        # get all audtions by part_id
        # nest by actor, should only be single actors... so easy.

    end

    def create # create audition
        # actual username findage @current_user and all that. 
        @user = User.find(params[:user_id])

        @audition = Audition.create!({
            user_id: @user.id,
            part_id: params[:part_id],
            submitted: false
        })

        if @audition.save
            p @audition
        else
            render_object({message: "An error has occoured"}) and return
        end

        @blobs = params[:blob_objects]
        @blobs.each_with_index do |blob, index|
            file_name = "#{@user.username}_#{params[:part_id]}_#{params[:line_ids][index]}_#{params[:take_indexes][index]}_#{Time.now.to_i}.weba"
            file_path = "public/#{file_name}"
            save_path = Rails.root.join(file_path)

            audio = blob
            audio.rewind
          
            File.open(save_path, 'wb') do |f|
                f.write audio.read
            end
            # write the file to disk. 
            # save the file path

            @audio_take = AudioTake.new({
                line_id: params[:line_ids][index],
                file_path: file_name
            })

            @audition.audio_takes << @audio_take

        end
        render_object(@audition.as_api_response(:saved_audition)) and return

    end

    def update
    end

    def destroy
    end

    def submit_audition
    end

    private

    def get_resource
        @user = User.find(params[:user_id])
        Audition.where(user_id: @user.id)
    end

end
