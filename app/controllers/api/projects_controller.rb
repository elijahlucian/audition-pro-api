class Api::ProjectsController < ApplicationController

    def index
        @projects = Project.all
        render_object(@projects.as_api_response(:projects))    
    end


end
