class Api::CommentsController < ApplicationController

    skip_before_action :verify_authenticity_token   

    def index
        
        @comments = Comment.where(user_id: params[:user_id])
    
    end

    def create

        @comment = Comment.new({
            user_id: params[:user_id],
            audio_take_id: params[:audio_take_id],
            body: params[:body]
        })

        @audition = AudioTake.find(params[:audio_take_id]).audition

        render_object(@audition.as_api_response(:saved_audition)) if @comment.save

    end

end
