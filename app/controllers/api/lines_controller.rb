class Api::LinesController < ApplicationController

    def show
        @lines = Line.find_by(part_id: params[:part_id])
        render_object(@lines.as_api_response(:lines))
    end

end
