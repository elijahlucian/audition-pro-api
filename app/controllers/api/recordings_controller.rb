class Api::RecordingsController < ApplicationController
    skip_before_action :verify_authenticity_token  

    require 'wavefile'
    include WaveFile

    def index
        @user = User.find_by(username: params['loggedInUser'])

        # get user id
        # get currentPart
        # populate auditions array
        # return array
        
        auditions = [Audition.all]
        render json: auditions
    end

    def create
        # p request.headers
        # find user by username

        @user = User.find_by(username: params['loggedInUser'])
        if @user.nil?
            render json: {error: 404}
        end
        audio = params['audio']
        audio.rewind
        
        filename = "#{params['filename']}.weba"
        file_path = "public/#{@user.username}/#{filename}"
        save_path = Rails.root.join(file_path)
        
        File.open(save_path, 'wb') do |f|
            f.write audio.read
        end
        
        @audition = Audition.new
        @audition.character_name = params['characterName']
        @audition.file_path = "#{filename}"
        # @audition.user_id = @user.id

        p"**************"
        p @user
        p @audition

        if @user.auditions << @audition
            render json: ["save","successful"]
        else
            render json: [:shit,:went,:wrong]
        end
    end

end
