class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception

  def index

  end

  def current_user
    return unless session[:username]
    @current_user ||= User.find(session[:username])
    
  end

  def render_object(object)
    render json: object, status: :ok
  end

end
