class CreateComments < ActiveRecord::Migration[5.1]
  def change
    create_table :comments, id: :uuid do |t|
      t.belongs_to :audio_take, foreign_key: {on_delete: :cascade}, type: :uuid
      t.belongs_to :user, foreign_key: {on_delete: :cascade}, type: :uuid
      t.string :body
      t.timestamps
    end
  end
end
