class CreateProjects < ActiveRecord::Migration[5.1]
  def change
    create_table :projects, id: :uuid do |t|
      t.belongs_to :user, foreign_key: {on_delete: :cascade}, type: :uuid, 
      t.string :name
      t.string :synopsis
      t.string :extra_info
      t.string :kind # [game, radioplay, gamertag, voice pack]
      t.timestamps
    end
  end
end
