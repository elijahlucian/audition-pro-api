class CreateVoiceTypes < ActiveRecord::Migration[5.1]
  def change
    create_table :voice_types, id: :uuid do |t|
      t.string :kind
      t.timestamps
    end
  end
end
