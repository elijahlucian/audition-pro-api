class CreateAudioTakes < ActiveRecord::Migration[5.1]
  def change
    create_table :audio_takes, id: :uuid do |t|
      t.belongs_to :audition, foreign_key: {on_delete: :cascade}, type: :uuid
      t.belongs_to :line, foreign_key: {on_delete: :cascade}, type: :uuid
      t.string :file_path
      t.timestamps
    end
  end
end
