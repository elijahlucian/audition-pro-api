class CreateParts < ActiveRecord::Migration[5.1]
  def change
    create_table :parts, id: :uuid do |t|
      t.belongs_to :project, foreign_key: {on_delete: :cascade}, type: :uuid

      t.string :character_name
      t.string :character_profile
      t.string :voice_type
      t.string :age
      t.string :accent
      t.string :notes
      t.string :reference

      t.timestamps
    end
  end
end
