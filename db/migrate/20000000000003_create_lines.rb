class CreateLines < ActiveRecord::Migration[5.1]
  def change
    create_table :lines, id: :uuid do |t|
      t.belongs_to :part, foreign_key: {on_delete: :cascade}, type: :uuid 
      t.string :text
      t.string :context
      t.string :file_name
      t.timestamps
    end
  end
end
