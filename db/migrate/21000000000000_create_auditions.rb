class CreateAuditions < ActiveRecord::Migration[5.1]
  def change
    create_table :auditions, id: :uuid do |t|
      t.belongs_to :part, foreign_key: {on_delete: :cascade}, type: :uuid
      t.belongs_to :user, foreign_key: {on_delete: :cascade}, type: :uuid
      t.boolean :submitted
      t.timestamps
    end
  end
end