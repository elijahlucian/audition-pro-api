# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

def actor_o_matic(username, email)
    u = User.new
    u.username = username
    u.email = email
    u.kind = 'actor'
    u.save!
end

def creator_o_matic(creators)

end

def voice_type_o_matic(voice_type)
    voice_type.each do |voice|
        VoiceType.create(kind: voice)
    end
end

actor_list = [ 
    [ 'elijahlucian', 'elijahlucian@gmail.com' ],
    [ 'thepinupgamer', 'kaelacaron@gmail.com' ],
    [ 'cytokinesis', 'eric@eviusstudios.com' ],
    [ 'LtSnakePlissken', 'shylocliffe@gmail.com' ],
    [ 'randolph', 'r@ndolph.ca' ],
    [ 'vmhutton', 'victoriah.marie@gmail.com' ],
    [ 'exdevlin', 'exdevlin@gmail.com' ],
    [ 'dawnbug', 'dawnbugandharry@gmail.com' ],
    [ 'meckstadt', 'meckstadt@gmail.com' ],
    [ 'Yeats', 'yeatsw@gmail.com' ],
    [ 'Elle', 'ellenguyuen.yyc@gmail.com'],
    [ 'AcemanR', 'rylandobloom@gmail.com']

]

p "creating creators..."

user_1 = User.create({
    id: '098765b0-4321-4ea1-b6b4-8fe240f9d1c7',
    username: 'charles@coffeeaddictstudio.com',
    email: 'charles@coffeeaddictstudio.com',
    kind: 'creator'
})

user_2 = User.create({
    id: '122d3237-537e-4e90-b409-5460ee4d79a9',
    username: 'fmunoz@aonegames.com',
    email: 'fmunoz@aonegames.com',
    kind: 'creator'
})

voice_type_o_matic(['gravelly', 'raspy', 'smooth', 'high', 'effeminate'])

p 'creating actors...'

actor_list.each do |actor|
    actor_o_matic(actor[0], actor[1])
end

p 'creating project 1'

project1 = Project.new
project1.name = 'Hazel Sky'
project1.user_id = user_1.id
project1.extra_info = "https://drive.google.com/open?id=14afQKP354qMNYIJpqIWLAAyXHMhN78oD"
project1.synopsis = 'Hazel Sky is a game about a boy trying to go back to his home in a floating city. Everyone that wants to become a engineer goes through that in this world, the entire society is devided into Artist(Really poor) and Engineers(Rich). The entire game is while you resolve really light puzzles like the game Inside (Main inspiration for the mechanics) the boy finding out that he realy is a musican, a much hated artist, the game deals with prejudice from many perspectives'
p project1
project1.save!

p 'adding part 1...'

part1 = Part.new
part1.project_id = project1.id
part1.character_name = 'Erin'
part1.character_profile = 'Girly, not too cartoony'
part1.age = 13
part1.voice_type = ''
part1.accent = 'n. american'
part1.reference = 'link to youtube or mp3'
part1.save!

p 'adding lines...'

Line.create [
    {part_id: part1.id, text: 'Yeah! That’s great, I haven’t talk to someone for months.', context: 'she is lonely'},
    {part_id: part1.id, text: 'Yeah I know, but they left all those radios from the time the proving had mentors.', context: 'she is lonely'}
]

p 'adding part 2...'

part2 = Part.new 
part2.project_id = project1.id
part2.character_name = 'Shane'
part2.character_profile = ''
part2.age = 12
part2.voice_type = ''
part2.accent = 'n. american'
part2.reference = 'link to youtube or mp3'
part2.save!

p 'adding lines...'

Line.create [
    {part_id: part2.id, text: 'You know we shouldn’t be talking right?', context: 'hesitant'},
    {part_id: part2.id, text: 'Nothing, I need to go, we can talk later, I need to get going.', context: 'matter-of-fact'}
]

p 'adding project 2...'

project2 = Project.new 
project2.name = 'Omen Of Sorrow'
project2.user_id = user_2.id
project2.synopsis = 'Omen of Sorrow is a classic 2D, four-button fighting game, with Unreal Engine 4-powered graphics, a cast of 12 characters inspired by classical horror, fantasy and mythology'
project2.save!

p 'adding part 1...'

part3 = Part.new 
part3.project_id = project2.id
part3.character_name = 'Arctorious'
part3.character_profile = 'Noble, ancient, mysterious'
part3.age = 999
part3.voice_type = 'Deep, serene, enigmatic'
part3.accent = 'Welsh'
part3.reference = 'http://some.youtube.link'
part3.save!

p 'adding lines...'

Line.create [
    {part_id: part3.id, text: 'Relent!', context: 'throws a powerful swipe directly in front of him'},
    {part_id: part3.id, text: 'Avalon, guide me!', context: 'activates a powered state'},
    {part_id: part3.id, text: '<Attack Sound>', context: 'Large effort (Medium. About .6 seconds)'}
]

p 'adding part 2...'

part4 = Part.new
part4.project_id = project2.id
part4.character_name = 'Quasimodo, the Watcher'
part4.character_profile = 'Pessimistic, skittish, angry'
part4.voice_type = 'Nervous, defensive, jittery'
part4.age = 250
part4.accent = 'French'
part4.notes = ''
part4.reference = 'http://some.otheryoutube.link'
part4.save!

p 'adding lines...'

Line.create [
    {part_id: part4.id, text: 'Back Off!', context: 'shoots his harpoon towards is opponent'},
    {part_id: part4.id, text: 'For Esmeralda!', context: 'activates a powered state'},
    {part_id: part4.id, text: '<Cry Of Pain>', context: 'screams out in agony as he receives terrible damage'}
] 

p 'done!'