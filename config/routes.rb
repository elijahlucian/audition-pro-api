Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html


  # :user_id is our 'logged in user'

  namespace :api do
    #resources :user do
    get   'user/:user_id'                    => 'users#show'
    get   'users'                             => 'users#index'

    # post  'audition/:user_id/:line_id'        => 'auditions#create'
    # get   'auditions/:user_id'                => 'auditions#auditons_by_actor' # for actors
    # get   'auditions/:part_id'                => 'auditions#auditons_by_part' # for creators

    post  'audition/:user_id/:part_id'        => 'auditions#create'
    get   'audition'                          => 'auditions#index'
    get   'audition/:user_id/:part_id'        => 'auditions#show'

    get   'comments/:user_id/:audition_id'    => 'comments#index'
    post  'comments/:user_id/:audio_take_id'  => 'comments#create'

    get   'projects'                          => 'projects#index'

    get   'parts'                             => 'parts#index'

    get   'lines/:part_id'                    => 'lines#show'

    get   'media/:take_id'                    => 'audio_takes#serve_file'

  end

end
